using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Mvc;

namespace ExampleRESTAPI.Controllers
{
    [Route("api/[controller]")]
    public class PeopleController : Controller
    {
        public class Person
        {
            public int id { get; set; }
            public string name { get; set; }
            public int age { get; set; }
            public string sex { get; set; }
            public string jobTitle { get; set; }
            public List<Person> relations { get; set; }

        }

        public List<Person> people = new List<Person>();

        public PeopleController()
        {
            var nathan = new Person
            {
                id = 1,
                name = "Nathan Thomas",
                age = 29,
                relations = new List<Person>()
            };
            var jackie = new Person
            {
                id = 3,
                name = "Jackie",
                relations = new List<Person>()
            };
            nathan.relations.Add(jackie);

            people.Add(nathan);


            people.Add(new Person
            {
                id = 2,
                name = "Charlie Lee",
                age = 90,
                relations = new List<Person>()
            });
            people.Add(jackie);

        }
        
        // GET: api/people
        [HttpGet]
        public List<Person> Get()
        {
            return people;
        }

        // GET api/people/5
        [HttpGet("{id}")]
        public Person Get(int id)
        {
            return people.Where(p => p.id == id).FirstOrDefault();
        }

        // GET api/people/5
        [HttpGet("{id}/relations")]
        public List<Person> GetRelations(int id)
        {
            return people.Where(p => p.id == id).OrderBy(p => p.age).FirstOrDefault().relations.ToList();
        }

        // POST api/people
        [HttpPost]
        public List<Person> Post([FromBody]Person person)
        {
            people.Add(person);
            return people;
        }

        // PUT api/people/5
        [HttpPut("{id}")]
        public Person Put(int id, [FromBody]Person data)
        {
            var person = people.Where(p => p.id == id).FirstOrDefault();
            person.name = data.name;
            person.age = data.age;
            return person;
        }
    }
}
